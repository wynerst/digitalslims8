# DigitalSLiMS8

SLiMS 8 Akasia - view digital collections mode
===============
SENAYAN Library Management System (SLiMS) version 8 Codename Akasia

SLiMS is free open source software for library resources management
(such as books, journals, digital document and other library materials)
and administration such as collection circulation, collection management,
membership, stock taking and many other else.

SLiMS is licensed under GNU GPL version 3. Please read "GPL-3.0 License.txt"
to learn more about GPL.

This is a front end only that limit all activities on digital collections (biblio with attachment only)
